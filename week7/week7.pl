use strict;
use warnings;

print "Hello World! \n";

my $scaler_variable = 5;
print 'The value of $scaler_variable is: ' . $scaler_variable . "\n";

$scaler_variable = 'This is a string';
print 'The value of $scaler_variable is now: ' . $scaler_variable . "\n";

my @a = (10, 20, 30);

@a = (10, "string", 3.14, $scaler_variable );

print "@a\n";
