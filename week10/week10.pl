use strict;
use warnings;

my $filename = $ARGV[0];

open IN, "<", $filename or die "Can no pen the file $filename: $!";

my @lines = <IN>;

close IN;

foreach my $line (@lines) {
  chomp $line;
  my @cols = split(";", $line);
  print("$cols[1]\t$cols[4]\t$cols[5]\n");
}
